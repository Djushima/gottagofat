﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Food : MonoBehaviour
{
    public int Value;
    public AudioClip type;

    private void Start()
    {
        StartCoroutine("Depop");
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        GameObject.Find("SFX").GetComponent<AudioSource>().PlayOneShot(type);

        if (other.gameObject.GetComponent<PlayerController1>() != null)
        {
            GameObject.Find("GameManager").GetComponent<GameManager>().Score1 += Value;
            Destroy(this.gameObject);
        }
        if (other.gameObject.GetComponent<PlayerController2>() != null)
        {
            GameObject.Find("GameManager").GetComponent<GameManager>().Score2 += Value;
            Destroy(this.gameObject);
        }
        if (other.gameObject.GetComponent<PlayerController3>() != null)
        {
            GameObject.Find("GameManager").GetComponent<GameManager>().Score3 += Value;
            Destroy(this.gameObject);
        }
        if (other.gameObject.GetComponent<PlayerController4>() != null)
        {
            GameObject.Find("GameManager").GetComponent<GameManager>().Score4 += Value;
            Destroy(this.gameObject);
        }
    }

    IEnumerator Depop()
    {
        yield return new WaitForSecondsRealtime(6);
        Destroy(this.gameObject);
    }
}
