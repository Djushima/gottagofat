﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController1 : MonoBehaviour
{
    public float x, y, jump, dash, DashTime = 0.4f, MaxDashTime = 0.4f;
    public int moveSpeed = 7, jumpForce = 320, dashForce = 20;
    public bool InJump, InDash, OnGround, DashStop;
    public AudioClip Dash;
    public ParticleSystem DashFx, BumpFx, ExplosionFX;

    Rigidbody2D m_Rigidbody;
    Animator m_anim;

    // Start is called before the first frame update
    void Start()
    {
        m_Rigidbody = this.gameObject.GetComponent<Rigidbody2D>();
        m_anim = this.gameObject.GetComponent<Animator>();
        ExplosionFX = GameObject.Find("DieParticule1").GetComponent<ParticleSystem>();
    }

    private void FixedUpdate()
    {
        Ray();
    }

    // Update is called once per frame
    void Update()
    {
        x = Input.GetAxis("Horizontal1");
        jump = Input.GetAxis("Jump1");
        dash = Input.GetAxis("Dash1");

        if (Mathf.Abs(x) > 0)
            transform.rotation = Quaternion.Euler(0, x >= 0 ? 0 : 180, 0);

        if (Mathf.Abs(x) > 0.9)
            m_anim.SetBool("Run", true);
        else
            m_anim.SetBool("Run", false);

        if (OnGround)
        {
            m_Rigidbody.velocity = new Vector2(x * moveSpeed, m_Rigidbody.velocity.y);
        }
        else if (Mathf.Abs(x) > 0)
        {
            m_Rigidbody.velocity = (new Vector2(x * moveSpeed / 2, m_Rigidbody.velocity.y));
        }


        if (jump > 0.2 && !InJump && OnGround)
        {
            Debug.Log("jump");
            InJump = true;
            m_Rigidbody.AddForce(new Vector2(0, jump) * jumpForce);
        }

        if (dash > 0.2 && !InDash)
        {
            InDash = true;
            DashTime = 0.0f;
            m_Rigidbody.velocity = Vector2.zero;
            GameObject.Find("SFX").GetComponent<AudioSource>().PlayOneShot(Dash);
            DashFx.transform.rotation = Quaternion.Euler(0,0,transform.rotation.y == 0 ? 0 : 180);
            DashFx.Play();
        }

        if (DashTime < MaxDashTime)
        {
            m_Rigidbody.velocity = new Vector2(transform.rotation.y == 0 ? dashForce : -dashForce, 0);
            DashTime += 0.1f;
        }
        else if (DashTime >= MaxDashTime && InDash && !DashStop)
        {
            DashStop = true;
            m_Rigidbody.velocity += (-m_Rigidbody.velocity) * 0.9f;
        }

        if (dash <= 0.1)
        {
            m_anim.SetBool("Dash", false);
            InDash = false;
            DashStop = false;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            InJump = false;
        }
        if (collision.gameObject.name == "platformMouvente")
        {
            transform.parent = collision.transform;
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.name == "platformMouvente")
        {
            transform.parent = null;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
            if (InDash)
            {
                Debug.Log("BUMP");
                BumpFx.Play();
                collision.gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                collision.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(transform.rotation.y == 0 ? 200 : -200, 200f));
            }
    }


    void Ray()
    {
        string[] MaskNameList = new string[] { "Ground" };
        int mask = LayerMask.GetMask(MaskNameList);

        RaycastHit2D Ground = Physics2D.Raycast(this.gameObject.transform.position, Vector2.down, 0.6f, mask);
        OnGround = Ground;

        Debug.DrawRay(this.gameObject.transform.position, Vector2.down * 0.6f, Color.red);
    }
}
