﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class platformMouvente : MonoBehaviour
{
    Vector3 pos;
    [SerializeField] float temps = 1f, envergure = 5f;
    [SerializeField] bool moveVertical;
    
    void Start()
    {
        pos = transform.position;
    }

    
    void Update()
    {
        if(!moveVertical){
            transform.position = new Vector3(pos.x + Mathf.PingPong(Time.time / temps, envergure), pos.y, pos.z);
        }
        else if (moveVertical)
        {
            transform.position = new Vector3(pos.x, pos.y + Mathf.PingPong(Time.time / temps, envergure), pos.z);
        }
    }

}
