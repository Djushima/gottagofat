﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Deathwalls : MonoBehaviour
{
    public AudioClip Explosion;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            GameObject.Find("SFX").GetComponent<AudioSource>().PlayOneShot(Explosion);
            switch(collision.gameObject.name)
            {
                case "Joueur1":
                    collision.gameObject.GetComponent<PlayerController1>().ExplosionFX.transform.position = collision.GetContact(0).point;
                    collision.gameObject.GetComponent<PlayerController1>().ExplosionFX.transform.localRotation = Quaternion.Euler(0, 0, FXrot(collision.GetContact(0)));
                    collision.gameObject.GetComponent<PlayerController1>().ExplosionFX.Play();
                    break;

                case "Joueur2":
                    collision.gameObject.GetComponent<PlayerController2>().ExplosionFX.transform.position = collision.GetContact(0).point;
                    collision.gameObject.GetComponent<PlayerController2>().ExplosionFX.transform.localRotation = Quaternion.Euler(0, 0, FXrot(collision.GetContact(0)));
                    collision.gameObject.GetComponent<PlayerController2>().ExplosionFX.Play();
                    break;

                case "Joueur3":
                    collision.gameObject.GetComponent<PlayerController3>().ExplosionFX.transform.position = collision.GetContact(0).point;
                    collision.gameObject.GetComponent<PlayerController3>().ExplosionFX.transform.localRotation = Quaternion.Euler(0, 0, FXrot(collision.GetContact(0)));
                    collision.gameObject.GetComponent<PlayerController3>().ExplosionFX.Play();
                    break;

                case "Joueur4":
                    collision.gameObject.GetComponent<PlayerController4>().ExplosionFX.transform.position = collision.GetContact(0).point;
                    collision.gameObject.GetComponent<PlayerController4>().ExplosionFX.transform.localRotation = Quaternion.Euler(0, 0, FXrot(collision.GetContact(0)));
                    collision.gameObject.GetComponent<PlayerController4>().ExplosionFX.Play();
                    break;
            }
            GameObject.Find("GameManager").GetComponent<GameManager>().Respawn(collision.gameObject);
        }
       
    }

    float FXrot(ContactPoint2D collision)
    {
        if (collision.point.x < -11.5)
            return -90;

        if (collision.point.x > 11.5)
            return 90;

        if (collision.point.y > 7.5)
            return 180;

        if (collision.point.y < -7.5)
            return 0;

        return 0;

    }
}
