﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangementPlatform : MonoBehaviour
{
    [SerializeField] GameObject[] map;
    [SerializeField] GameObject piege,exclamation;
    private int platFormDes = -1,randPlatform;
    [SerializeField] int timingChangement,nombrePlatform;
    Vector3 posPlatform;

    void Start()
    {

    }

    
    void Update()
    {

    }
    IEnumerator ChangePlatform ()
    {
        yield return new WaitForSeconds(3.0f);
        while (true)
        {
            randPlatform = Random.Range(0, nombrePlatform);
            while (randPlatform == platFormDes)
            {
                randPlatform = Random.Range(0, nombrePlatform);
            }
            posPlatform = map[randPlatform].transform.position;
            platFormDes = randPlatform;
            Instantiate(exclamation , new Vector3(posPlatform.x, posPlatform.y +0.8f, posPlatform.x), Quaternion.identity);
            yield return new WaitForSeconds(1.3f);
            var instance = Instantiate(piege, new Vector3(posPlatform.x, posPlatform.y-0.05f, posPlatform.x), Quaternion.identity);
            yield return new WaitForSeconds(timingChangement);
            instance.GetComponent<SpikeSpawn>().Destroy();
        }
    }
    public void StartChangePlatform()
    {
        StartCoroutine("ChangePlatform");
    }
    public void StopChangePlatform()
    {
        StopCoroutine("ChangePlatform");
    }
}
