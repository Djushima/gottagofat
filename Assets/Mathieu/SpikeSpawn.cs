﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikeSpawn : MonoBehaviour
{
    void Start()
    {
        StartCoroutine("Spawn");
    }

    void Update()
    {
        
    }
    public void Destroy()
    {
        StartCoroutine("DestroySpike");
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player") GameObject.Find("GameManager").GetComponent<GameManager>().Respawn(collision.gameObject);

    }
    IEnumerator Spawn ()
    {
        for (int i = 0; i < 10; i++)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y+0.03f, transform.position.z);
            yield return new WaitForSeconds(0.05f);
        }
    }

    IEnumerator DestroySpike()
    {
        for (int i = 0; i < 10; i++)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y - 0.03f, transform.position.z);
            yield return new WaitForSeconds(0.05f);
        }
        Destroy(gameObject);
    }
}
