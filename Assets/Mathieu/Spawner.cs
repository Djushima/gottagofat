﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject[] aliments ;
    [SerializeField] GameObject platformMouvente;
    [SerializeField] float xMin, xMax;
    [SerializeField] int timingSpawn;
    int randValue,randY;
    float posX, posY;

    void Start()
    {
        
    }

    public void StartSpawn()
    {
        StartCoroutine("Spwaner");
        platformMouvente.SetActive(true);
    }
    public void StopSpawn()
    {
        StopCoroutine("Spwaner");
        platformMouvente.SetActive(false);
    }

    public IEnumerator Spwaner()
    {
        while (true)
        {
            
            yield return new WaitForSeconds(Random.Range(timingSpawn + (timingSpawn/2), timingSpawn - (timingSpawn / 2)));
            randValue = Random.Range(0, 13);
            if (randValue == 6 || randValue == 7 || randValue == 8) randValue = 0;
            else if (randValue == 9 || randValue == 10) randValue = 1;
            else if (randValue == 11) randValue = 2;
            else if (randValue == 12) randValue = 4;
            randY= Random.Range(0, 2);
            if (randY == 0)
            {
                posY =4;
                posX = Random.Range(xMin, xMax);
            }
            else if(randY == 1)
            {
                posY =-0.1f;
                posX = Random.Range(xMin, xMax);
                while (posX < -1.5f && posX > 0.75f)
                {
                    posX = Random.Range(xMin, xMax);
                    yield return new WaitForSeconds(0.01f);
                }
            }
            Instantiate(aliments[randValue] , new Vector3(posX, posY, transform.position.z), Quaternion.identity);
        }
    }
}
