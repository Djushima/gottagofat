﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public int Score1, Score2, Score3, Score4;
    public bool p2, p3, p4 = false;
    [SerializeField] int Time;
    List<int> winners = new List<int>();
    [SerializeField] Text timetext;
    [SerializeField] GameObject Menu;
    public GameObject Lobby;
    public bool inlobby = true;
    [SerializeField] Text[] texts = new Text[3];
    GameObject Joueur1, Joueur2, Joueur3, Joueur4;
    [SerializeField] int Pointsperdus;
    [SerializeField] GameObject Characters;
    [SerializeField] GameObject Characters2;
    GameObject a, b, c, aa, ba, ca;

    private void Start()
    {
        a = Characters2.transform.Find("2").gameObject;a.SetActive(false);
        b = Characters2.transform.Find("3").gameObject;b.SetActive(false);
        c = Characters2.transform.Find("4").gameObject;c.SetActive(false);
        aa = Characters.transform.Find("2").gameObject; aa.SetActive(false);
        ba = Characters.transform.Find("3").gameObject; ba.SetActive(false);
        ca = Characters.transform.Find("4").gameObject; ca.SetActive(false);
    }
    private void Update()
    {

        if(inlobby == false && (Input.GetKeyDown(KeyCode.Joystick1Button7) || Input.GetKeyDown(KeyCode.Joystick2Button7) || Input.GetKeyDown(KeyCode.Joystick3Button7) || Input.GetKeyDown(KeyCode.Joystick4Button7)))
        {
            if (Menu.activeSelf == true) { Menu.SetActive(false); UnityEngine.Time.timeScale = 1; }
            else { Menu.SetActive(true); UnityEngine.Time.timeScale = 0; }
        }

        if(inlobby  && Input.GetKeyDown(KeyCode.Joystick1Button0) && (p2 || p3 || p4))
        {
            Lobby.SetActive(false);
            inlobby = false;
            StartTimer();
        }

        if(Score1 < 0) { Score1 = 0; }
        if (Score2 < 0) { Score2 = 0; }
        if (Score3 < 0) { Score3 = 0; }
        if (Score4 < 0) { Score4 = 0; }

        if (inlobby && Input.GetKeyDown(KeyCode.Joystick2Button7))
        {
            p2 = true;
            aa.SetActive(true);
            a.SetActive(true);
            texts[0].text = "READY";
        }

        if (inlobby && Input.GetKeyDown(KeyCode.Joystick3Button7))
        {
            p3 = true; b.SetActive(true);
            ba.SetActive(true);
            texts[1].text = "READY";
        }

        if (inlobby && Input.GetKeyDown(KeyCode.Joystick4Button7))
        {
            p4 = true;
           ca.SetActive(true); c.SetActive(true);
            texts[2].text = "READY";
        }

        if (!inlobby)
        {
            Characters2.transform.Find("1").Find("Text").GetComponent<Text>().text = Score1.ToString();
            if (p2) Characters2.transform.Find("2").Find("Text").GetComponent<Text>().text = Score2.ToString();
            if (p3) Characters2.transform.Find("3").Find("Text").GetComponent<Text>().text = Score3.ToString();
            if (p4) Characters2.transform.Find("4").Find("Text").GetComponent<Text>().text = Score4.ToString();
        }
    }

    public void Respawn(GameObject player)
    {
        string playername = player.name;
        Destroy(player);
        if (playername.Contains("1"))
        {
            if (Score1 <= Pointsperdus) { SpawnFruit(Score1); }
            else SpawnFruit(Pointsperdus);
            Score1 -= Pointsperdus;         
            StartCoroutine(Respawnplayer(playername));
        }
        if (playername.Contains("2"))
        {
            if (Score2 <= Pointsperdus) { SpawnFruit(Score2); }
            else SpawnFruit(Pointsperdus);
            Score2 -= Pointsperdus;
            StartCoroutine(Respawnplayer(playername));
        }
        if (playername.Contains("3"))
        {
            if (Score3 <= Pointsperdus) { SpawnFruit(Score3); }
            else SpawnFruit(Pointsperdus);
            Score3 -= Pointsperdus;
            StartCoroutine(Respawnplayer(playername));
        }
        if (playername.Contains("4"))
        {
            if (Score4 <= Pointsperdus) { SpawnFruit(Score4); }
            else SpawnFruit(Pointsperdus);
            Score4 -= Pointsperdus;
            StartCoroutine(Respawnplayer(playername));
        }
    }

    void SpawnFruit(int number)
    {
        for (int i = 1; i <= number; i++)
        {
            float posX = Random.Range(-7, 7);
            Instantiate(GameObject.Find("Spawner").GetComponent<Spawner>().aliments[0], new Vector3(posX, transform.position.y, transform.position.z), Quaternion.identity);
        }
    }

    IEnumerator Respawnplayer(string playername)
    {

        yield return new WaitForSecondsRealtime(1);
        if (playername.Contains("1"))
        {
            Instantiate(Resources.Load("Prefabs/Joueur1") as GameObject, new Vector3(Random.Range(-6, 6), 6, 0), Quaternion.identity);
        }
        else if (playername.Contains("2"))
        {
            Instantiate(Resources.Load("Prefabs/Joueur2") as GameObject, new Vector3(Random.Range(-6, 6), 6, 0), Quaternion.identity);
        }
        else if (playername.Contains("3"))
        {
            Instantiate(Resources.Load("Prefabs/Joueur3") as GameObject, new Vector3(Random.Range(-6, 6), 6, 0), Quaternion.identity);
        }
        else if (playername.Contains("4"))
        {
            Instantiate(Resources.Load("Prefabs/Joueur4") as GameObject, new Vector3(Random.Range(-6, 6), 6, 0), Quaternion.identity);
        }
    }
    public void GetWinners()
    {
        GameObject.Find("Spawner").GetComponent<Spawner>().StopSpawn();
        GameObject.Find("PlatformManager").GetComponent<ChangementPlatform>().StopChangePlatform();
        Joueur2 = GameObject.Find("Joueur2(Clone)");
        Joueur1 = GameObject.Find("Joueur1(Clone)");
        Joueur3 = GameObject.Find("Joueur3(Clone)");
        Joueur4 = GameObject.Find("Joueur4(Clone)");
        if(Joueur1 != null) Joueur1.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
        if (p2) { if (Joueur2 != null) Joueur2.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static; }
        if (p3) { if (Joueur3 != null) Joueur3.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static; }
        if (p4) { if (Joueur4 != null) Joueur4.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static; }
        winners.Clear();
        int bestscore = Mathf.Max(Score1, Score2, Score3, Score4);
        if (bestscore == Score1) winners.Add(1);
        if (bestscore == Score2) winners.Add(2);
        if (bestscore == Score3) winners.Add(3);
        if (bestscore == Score4) winners.Add(4);
    }

    public void StartTimer()
    {
        Joueur1 = Instantiate(Resources.Load("Prefabs/Joueur1") as GameObject, new Vector3(-5.5f, -2.1f, 0), Quaternion.identity);
        Joueur1.GetComponent<PlayerController1>().enabled = false;
        Joueur2 = GameObject.Find("Joueur2(Clone)");
        Joueur1 = GameObject.Find("Joueur1(Clone)");
        Joueur3 = GameObject.Find("Joueur3(Clone)");
        Joueur4 = GameObject.Find("Joueur4(Clone)");
        if (p2) { Joueur2 = Instantiate(Resources.Load("Prefabs/Joueur2") as GameObject, new Vector3(-3.5f, 1.5f, 0), Quaternion.identity); Joueur2.GetComponent<PlayerController2>().enabled = false; }
        if (p3) { Joueur3 = Instantiate(Resources.Load("Prefabs/Joueur3") as GameObject, new Vector3(3.2f, 1.5f, 0), Quaternion.identity); Joueur3.GetComponent<PlayerController3>().enabled = false; }
        if (p4) { Joueur4 = Instantiate(Resources.Load("Prefabs/Joueur4") as GameObject, new Vector3(5.5f, -2.1f, 0), Quaternion.identity); Joueur4.GetComponent<PlayerController4>().enabled = false; }
        StartCoroutine(Timer());
    }

    IEnumerator Timer()
    {
        for (int i = 3; i > -1; i--)
        {
            timetext.text = i.ToString() + "!";
            if (i == 0) timetext.text = "GO!";
            yield return new WaitForSecondsRealtime(1);
        }
        GameObject.Find("Spawner").GetComponent<Spawner>().StartSpawn();
        GameObject.Find("PlatformManager").GetComponent<ChangementPlatform>().StartChangePlatform();
        Joueur2 = GameObject.Find("Joueur2(Clone)");
        Joueur1 = GameObject.Find("Joueur1(Clone)");
        Joueur3 = GameObject.Find("Joueur3(Clone)");
        Joueur4 = GameObject.Find("Joueur4(Clone)");
        Joueur1.GetComponent<PlayerController1>().enabled = true;
        try { if (p2) { Joueur2.GetComponent<PlayerController2>().enabled = true;  } } catch{ }
        try { if (p3) { Joueur3.GetComponent<PlayerController3>().enabled = true;  } } catch { }
        try { if (p4) { Joueur4.GetComponent<PlayerController4>().enabled = true;  } } catch { }
        int CurrentTime = Time;
        timetext.text = CurrentTime.ToString();
        while (CurrentTime > 0)
        {
            yield return new WaitForSecondsRealtime(1);
            CurrentTime = CurrentTime - 1;
            timetext.text = CurrentTime.ToString();
        }
        Lobby.SetActive(true);
        texts[0].gameObject.SetActive(false);
        texts[1].gameObject.SetActive(false);
        texts[2].gameObject.SetActive(false);
        texts[3].gameObject.SetActive(false);
        Characters.SetActive(true);
        GetWinners();
        while(Score1>0 || Score2>0 || Score3 >0 || Score4>0)
        {
            if(Score1 > 0)
            {
                Score1 -= 1;
                Characters.transform.Find("1").localScale += new Vector3(0.02f, 0.02f, 0.02f);
            }
            if (Score2 > 0)
            {
                Score2 -= 1;
                Characters.transform.Find("2").localScale += new Vector3(0.02f, 0.02f, 0.02f);
            }
            if (Score3 > 0)
            {
                Score3 -= 1;
                Characters.transform.Find("3").localScale += new Vector3(0.02f, 0.02f, 0.02f);
            }
            if (Score4 > 0)
            {
                Score4 -= 1;
                Characters.transform.Find("4").localScale += new Vector3(0.02f, 0.02f, 0.02f);
            }
            yield return new WaitForSeconds(0.05f);
        }
        
        if (winners.Count == 1) timetext.text = "PLAYER " + winners[0].ToString() + " WIN";
        if (winners.Count == 2) timetext.text = "PLAYER " + winners[0].ToString() + " AND " + "PLAYER " + winners[1].ToString() + " WIN";
        if (winners.Count == 3) timetext.text = "PLAYER " + winners[0].ToString() + ", " + "PLAYER " + winners[1].ToString() + " AND " + "PLAYER " + winners[2].ToString() + " WIN";
        if (winners.Count == 4) timetext.text = "ALL PLAYER WIN";
    }

}