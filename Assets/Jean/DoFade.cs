﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DoFade : MonoBehaviour {

	// Use this for initialization
	void Start () {
        foreach (Button But in GameObject.FindObjectsOfType(typeof(Button)))
        {
            But.interactable = false;
        }
        GameObject.Find("FadeObject").GetComponent<Fader>().StartCoroutine("Unfade");
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
