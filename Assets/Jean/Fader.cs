﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;

public class Fader : MonoBehaviour {
    Color color;
    // Use this for initialization
    void Start () {
        color = this.gameObject.GetComponent<Image>().color;
	}
    public void Preparefade(string scene)
    {
        StartCoroutine(Fade(scene));
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public IEnumerator Fade(string scene)
    {
        foreach (Button But in GameObject.FindObjectsOfType(typeof(Button)))
        {
            But.interactable = false;
        }
        //GetComponent<AudioSource>().PlayOneShot(GetComponent<AudioSource>().clip);
        color = this.gameObject.GetComponent<Image>().color;
        while (color.a != 1f)
        {      
            color.a += 0.05f;
            if (color.a > 1f) color.a = 1f;
            this.gameObject.GetComponent<Image>().color = color;
            yield return new WaitForEndOfFrame();
        }
        Time.timeScale = 1;
        SceneManager.LoadScene(scene);
        yield return new WaitForSecondsRealtime(1);
        foreach (Button But in GameObject.FindObjectsOfType(typeof(Button)))
        {
            But.interactable = false;
        }
        while (color.a != 0f)
        {
            color.a -= 0.05f;
            if (color.a < 0f) color.a = 0f;
            this.gameObject.GetComponent<Image>().color = color;
            yield return new WaitForEndOfFrame();
        }
    }

    public IEnumerator Unfade()
    {
        StopCoroutine("Fade");
        color = this.gameObject.GetComponent<Image>().color;
        while (color.a != 0f)
        {           
            color.a -= 0.05f;
            if (color.a < 0f) color.a = 0f;
            this.gameObject.GetComponent<Image>().color = color;

            yield return new WaitForEndOfFrame();
        }

        foreach (Button But in GameObject.FindObjectsOfType(typeof(Button)))
        {
            But.interactable = true;
        }
    }
}
